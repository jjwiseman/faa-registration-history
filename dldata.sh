#!/bin/bash

# Copyright (c) 2018 Chris Kuethe <chris.kuethe@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#set -x

F=$(date "+ReleasableAircraft-%Y%m%d")

if [ ! -e .git ] ; then
	git init .
	echo '*.txt	text' > .gitattributes
	echo -e '*\n!*.sh\n!*.txt' > .gitignore
	git add -f dldata.sh .gitattributes .gitignore
	git commit -a -m "first commit"
fi

test -e "${F}.zip" || wget -O "${F}.zip" http://registry.faa.gov/database/ReleasableAircraft.zip

for F in *zip ; do
	# unzip the archive as text, ignoring directories, overwrite existing, case-insensitive filename matches, lowercase all filenames, only extract .txt files
	/usr/bin/unzip -aa -j -o -C -LL -U "${F}" '*.txt'

	for T in *.txt.txt ; do
		mv -f $T "${T%.txt}"
	done

	# strip trailing spaces in fields and fix line endings
	perl -spi -e "s/\s*,/,/g;" -e 's/\r$//;' *.txt

	git add *.txt
	git commit -m "updates from '${F}'" *txt
	rm $F
done 
